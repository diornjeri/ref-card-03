# Architecture Ref. Card 03

## Einleitung

Das ist eine Spring Boot Application mit einer MariaDB Database.
Die Aufgabe war es dieses Repository zu forken und die Tasks auf OneNote zu befolgen und zuerarbeiten. Danach mussten wir ein Readme File verfassen (Was Sie gerade am lesen sind), dass den ganzen Prozess aufzeigt und zusammenfasst um nachzuvollziehen wie man die Installation führen muss.
In diesem Readme File finden Sie genau das.
Im Folgenden werden die Schritte zur Ausführung der gewünschten Funktionalitäten mithilfe der Pipeline zusammengefasst.

## Benötigte Files und Aufsetzungen

Wir brauchen ein Dockerfile sowie ein .gitlab-ci-yml File um dieses Projekt zu nutzen. AUsserdem werden wir besonders viel auf AWS zugreifen müssen. Dort werden wir viel mit den verschiedenen Diensten arbeiten müssen, doch das kommt später im Laufe der Ausführung.

Was wir genau nutzen werden:

### Dienste auf AWS

- ECR (Registry)
- ECS (Cluster + Service)
- RDS (MariaDB)


### Dockerfile

```sh
FROM maven:3-openjdk-11-slim

COPY src /src
COPY pom.xml /
RUN mvn -f pom.xml clean package

RUN mv /target/*.jar app.jar
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "/app.jar"]
```

### .gitlab-ci.yml

```sh
image: docker:23.0.4

variables:
  DOCKER_HOST: tcp://docker:2375
  DOCKER_TS_CERTDIR: ""

services:
  - docker:23.0.4-dind

auto-deploy:
  stage: deploy
  before_script:
    - apk add --no-cache py3-pip
    - pip install awscli
    - aws --version

  script:
    - aws ecs update-service --cluster $AWS_ECS_CLUSTER_NAME --service $AWS_ECS_SERVICE_NAME --force-new-deployment

package:
  stage: build
  before_script:
    - apk add --no-cache py3-pip
    - pip install awscli
    - aws --version

    - aws ecr get-login-password | docker login --username AWS --password-stdin $CI_AWS_ECR_REGISTRY

  script:
    - docker build --cache-from $CI_AWS_ECR_REGISTRY/$CI_AWS_ECR_REPOSITORY_NAME:latest -t $CI_AWS_ECR_REPOSITORY_NAME .
    - docker tag $CI_AWS_ECR_REPOSITORY_NAME:latest $CI_AWS_ECR_REGISTRY/$CI_AWS_ECR_REPOSITORY_NAME:latest
    - docker push $CI_AWS_ECR_REGISTRY/$CI_AWS_ECR_REPOSITORY_NAME:latest
```

## Installation

1. Zuerst forkt man das Projekt, dass man bekommen hat.
2. Nun pusht man die vorher erstellten Files hoch.

```sh
git add .
```

```sh
git commit -m "<Kommentar>"
```

```sh
git push
```

3. Bei AWS erstellt man nun ein neues Repository im Abschnitt ECR.
4. Nun wechseln wir zu Docker. Wir erstellen das Docker Image lassen es laufen.
```sh
$ docker build -t refcard-03 .
```
```sh
$ docker run refcard-03 .
```
5. Unter Gitlab navigiert man zu den Settings des Repos und dann zum Abschnitt CI/CD. Unter Runners, erstellt man einen neuen Runner. Hier ist wichtig, dass 'Run untagged jobs' aktiviert ist. Das ist so, damit man Jobs starten kann die nicht bestimmte Tags haben.
6. Nun den Container auf Docker erstellen.

```sh
docker run -d --name gl-runner -v /var/run/docker.sock:/var/run/docker.sock gitlab/gitlab-runner
```

Dieser Command startet den Container namens 'gl-runner' und verwendet das gefügte Image.

```sh
docker exec gl-runner gitlab-runner register --url https://gitlab.com --token < TOKEN > --executor docker --docker-image docker:23.0.4 --docker-privileged --non-interactive
```

Dieser Command sorgt dafür, dass der erstellte Container sich mit dem Runner verbindet, der zuvor erstellt wurde.

```sh
docker restart gl-runner
```

7. Jetzt erstellt man die Variabeln unter dem Abschnitt Settings -> CI/CD -> Variables. Dafür nutzen wir folgende Variablen: (Wichtig ist noch 'Protect Variabel abzukreuzen')

   | Gitlab Variable | Beschreibung |  
   | -------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
   | AWS_ACCESS_KEY_ID | Access Key ID von der learner lab CLI |  
   | AWS_DEFAULT_REGION | Region von AWS |  
   | AWS_SECRET_ACCESS_KEY | AWS Secret Access Key von der learner lab CLI |  
   | AWS_SESSION_TOKEN | AWS Session Token von der learner lab CLI | |
   | CI_AWS_ECR_REPOSITORY_NAME | Name des Repositories, dass in AWS erstellt wurde |  
   | CI_AWS_ECR_REGISTRY | Name des Registries, dass in AWS erstellt wurde. |  
   | CD_AWS_ECS_SERVICE | Name des AWS Services (Wird für Deployment benötigt) |  
   | CD_AWS_ECS_CLUSTER | Name des AWS Clusters (Wird für Deployment benötigt) |

8. Wie oben bei der Beschreibung beschrieben muss man für die ersten 3 Variabeln muss man auf AWS gehen und im Learner Lab unter AWS Details und auf AWS CLI navigieren um die Zugangsdaten zu finden. Für die restlichen Variabeln die übrig bleiben, schaut man bei seinem erstelltem Repository nach den Zugangsdaten. Am besten kann man das am URL Pfad ablesen vom Repository.

9. Jetzt erstellen wir ein Cluster bei ECS in AWS. Als Hilfe kann man die alten Files benutzen die auf OneNote sind.
10. Hier erstellen wir dann die Aufgabendefinitionen, dazu kann man folgende Umgebungsvariablen setzen:

    | Variable    | Default (Falls nicht gesetzt)       |
    | ----------- | ----------------------------------- |
    | DB_URL      | jdbc:mariadb://database:3306/jokedb |
    | DB_USERNAME | jokedbuser                          |
    | DB_PASSWORD | 123456                              |

11. Nun ist es wichtig auch noch eine DB bei RDS zu erstellen.

### Dockerfile ergänzen 

Jetzt müssen wir die Umgeungsvariabeln, die vorher definiert wurden in der Aufgabendefinition, zum Dockerfile ergänzen. 

Das sollte etwa so aussehen: 

    `ENV DB_URL=$DB_URL` `ENV DB_USER=$DB_USERNAME` `ENV DB_PASS=$DB_PASSWORD`
